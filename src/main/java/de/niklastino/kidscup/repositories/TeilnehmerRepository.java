package de.niklastino.kidscup.repositories;

import de.niklastino.kidscup.entities.Teilnehmer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TeilnehmerRepository extends MongoRepository<Teilnehmer, String> {
}
