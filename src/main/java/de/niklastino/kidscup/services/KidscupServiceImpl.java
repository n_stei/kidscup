package de.niklastino.kidscup.services;

import de.niklastino.kidscup.entities.Teilnehmer;
import de.niklastino.kidscup.repositories.TeilnehmerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KidscupServiceImpl implements KidscupService{

    private final TeilnehmerRepository teilnehmerRepository;

    @Autowired
    public KidscupServiceImpl(final TeilnehmerRepository teilnehmerRepository) {
        this.teilnehmerRepository = teilnehmerRepository;
    }

    @Override
    public Teilnehmer getTeilnehmer(String id) {
        return teilnehmerRepository.findById(id);
    }

    @Override
    public List<Teilnehmer> getAllTeilnehmer() {
        return null;
    }

    @Override
    public String createTeilnehmer(String name, String vorname) {
        return null;
    }

    @Override
    public Teilnehmer updateTeilnehmer(Teilnehmer teilnehmer, String oldTeilnehmerId) {
        return null;
    }

    @Override
    public void deleteTeilnehmer(String id) {

    }

    @Override
    public List<Teilnehmer> getTeilnehmerByName(String name) {
        return null;
    }

    @Override
    public Teilnehmer getTeilnehmerById(String id) {
        return null;
    }
}
