package de.niklastino.kidscup.services;

import de.niklastino.kidscup.entities.Teilnehmer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KidscupService {

    Teilnehmer getTeilnehmer(String id);
    List<Teilnehmer> getAllTeilnehmer();
    String createTeilnehmer(String name, String vorname);
    Teilnehmer updateTeilnehmer(Teilnehmer teilnehmer, String oldTeilnehmerId);
    void deleteTeilnehmer(String id);
    List<Teilnehmer> getTeilnehmerByName(String name);
    Teilnehmer getTeilnehmerById(String id);
}
