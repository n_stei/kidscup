package de.niklastino.kidscup.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Teilnehmer {

    @Id
    String id;

    String name;

    String vorname;

    public Teilnehmer(final String name, final String vorname) {
        this.name = name;
        this.vorname = vorname;
    }

}
